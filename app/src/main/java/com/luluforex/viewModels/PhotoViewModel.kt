package com.luluforex.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.luluforex.db.PhotoRepository
import com.luluforex.model.PhotoResponse
import com.luluforex.networking.Repository
import com.luluforex.util.InternetCheck

class PhotoViewModel(application: Application) : AndroidViewModel(application) {
    private val dBphotoRepository: PhotoRepository
    private val networkRepository: Repository
    val photosDB: LiveData<List<PhotoResponse>>
    private val networkPhotos: MutableLiveData<List<PhotoResponse>>
    private val isLoading: MutableLiveData<Boolean>
    var isConnected: MutableLiveData<Boolean>
    fun insert(photos: List<PhotoResponse?>) {
        dBphotoRepository.insert(photos!!)
    }

    fun getPhotos(online: Boolean): MutableLiveData<List<PhotoResponse>> {
        return  networkPhotos
    }

    fun getPhotosDb(online: Boolean): LiveData<List<PhotoResponse>> {
        return  photosDB
    }

    fun IsLoading(): MutableLiveData<Boolean> {
        return isLoading
    }

    fun IsConnected(): MutableLiveData<Boolean> {
        return isConnected
    }

    init {
        dBphotoRepository = PhotoRepository(application)
        networkRepository = Repository()
        photosDB = dBphotoRepository.allPhotos
        networkPhotos = networkRepository.photos
        isLoading = networkRepository.isLoading
        isConnected = InternetCheck(application).connected
    }
}