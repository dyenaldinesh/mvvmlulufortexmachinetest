package com.luluforex.networking

import androidx.lifecycle.MutableLiveData
import com.luluforex.model.PhotoResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    private val albumApi: PhotoApi
    var isLoading: MutableLiveData<Boolean>

    val photos: MutableLiveData<List<PhotoResponse>>
        get() {
            isLoading.value = false
            val photoData = MutableLiveData<List<PhotoResponse>>()
            albumApi.getphotos().enqueue(object : Callback<List<PhotoResponse>?> {
                override fun onResponse(call: Call<List<PhotoResponse>?>, response: Response<List<PhotoResponse>?>) {
                    isLoading.value = true
                    if (response.isSuccessful) {
                        photoData.value = response.body()
                    }
                }

                override fun onFailure(call: Call<List<PhotoResponse>?>, t: Throwable) {
                    photoData.value = null
                    isLoading.value = true
                }
            })
            return photoData
        }

    companion object {
        private var repository: Repository? = null
        val instance: Repository?
            get() {
                if (repository == null) {
                    repository = Repository()
                }
                return repository
            }
    }

    init {
        albumApi = RetrofitService.cteateService(PhotoApi::class.java)
        isLoading = MutableLiveData()
    }
}