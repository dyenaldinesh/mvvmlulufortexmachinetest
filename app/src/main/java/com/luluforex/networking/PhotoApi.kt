package com.luluforex.networking

import com.luluforex.model.PhotoResponse
import retrofit2.Call
import retrofit2.http.GET

interface PhotoApi {
    @GET("photos")
    fun getphotos(): Call<List<PhotoResponse>>

}