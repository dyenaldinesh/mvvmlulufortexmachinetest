package com.luluforex

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.luluforex.adapter.PhotoAdapter
import com.luluforex.databinding.ActivityMainBinding
import com.luluforex.model.PhotoResponse
import com.luluforex.viewModels.PhotoViewModel
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    val photosArrayList: MutableList<PhotoResponse> = ArrayList()
    private var photoViewModel: PhotoViewModel? = null
    private var adapter: PhotoAdapter? = null
    private var binding: ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        photoViewModel = ViewModelProvider(this).get(PhotoViewModel::class.java)
        photoViewModel!!.IsLoading().observe(this, Observer { isLoading: Boolean? ->
            if (isLoading != null) {
                if (isLoading) {
                    binding!!.progressBar.visibility = View.GONE
                }
            }
        })
        photoViewModel!!.IsConnected().observe(this, Observer { isconnected ->
            if (isconnected) {
                Toast.makeText(applicationContext, "Online", Toast.LENGTH_SHORT).show()
                photoViewModel!!.getPhotos(isconnected).observe(this@MainActivity, Observer<List<PhotoResponse?>?> { photoResponse: List<PhotoResponse?>? ->
                    photosArrayList.addAll(photoResponse!!.filterNotNull())
                    adapter!!.notifyDataSetChanged()
                    InsertintoDb(photoResponse) })
            } else {
                Toast.makeText(applicationContext, "Offline", Toast.LENGTH_SHORT).show()
                photoViewModel!!.getPhotosDb(isconnected).observe(this@MainActivity, Observer<List<PhotoResponse?>?> { photoResponse: List<PhotoResponse?>? ->
                    photosArrayList.addAll(photoResponse!!.filterNotNull())
                    adapter!!.notifyDataSetChanged()
                }) }

        })
        setupRecyclerView()

    }

    private fun InsertintoDb(photoResponse: List<PhotoResponse?>) {
        photoViewModel!!.photosDB.observe(this@MainActivity, Observer<List<PhotoResponse?>?> { photoDB: List<PhotoResponse?>? ->
            if (photoDB!!.size == 0)
                photoViewModel!!.insert(photoResponse!!)
        })
    }

    private fun setupRecyclerView() {
        if (adapter == null) {
            adapter = PhotoAdapter(this, photosArrayList)
            binding!!.recyclerView.layoutManager = LinearLayoutManager(this)
            binding!!.recyclerView.adapter = adapter
            binding!!.recyclerView.itemAnimator = DefaultItemAnimator()
            binding!!.recyclerView.isNestedScrollingEnabled = true
        } else {
            adapter!!.notifyDataSetChanged()
        }
    }
}

