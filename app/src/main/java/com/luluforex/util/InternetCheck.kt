package com.luluforex.util

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.MutableLiveData

class InternetCheck(application: Application) {
    var connected: MutableLiveData<Boolean>

    fun isOnline(context: Context): MutableLiveData<Boolean> {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnectedOrConnecting) connected.setValue(true) else connected.setValue(false)
        return connected
    }

    init {
        connected = MutableLiveData()
        isOnline(application)
    }
}