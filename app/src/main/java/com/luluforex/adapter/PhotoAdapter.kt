package com.luluforex.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.luluforex.R
import com.luluforex.adapter.PhotoAdapter.PhotoViewHolder
import com.luluforex.model.PhotoResponse
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class PhotoAdapter(var context: Context, var photos: MutableList<PhotoResponse>) : RecyclerView.Adapter<PhotoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.photo_item, parent, false)
        return PhotoViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.txtDes.text = photos.get(position).title
        Picasso.get()
                .load(photos[position]!!.thumbnailUrl)
                .into(holder.imagePhoto, object : Callback {
                    override fun onSuccess() {
                        Picasso.get()
                                .load(photos[position]!!.url)
                                .into(holder.imagePhoto)
                    }

                    override fun onError(e: Exception) {}
                })
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    inner class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtDes: TextView
        var imagePhoto: ImageView

        init {
            txtDes = itemView.findViewById(R.id.txt_desCription)
            imagePhoto = itemView.findViewById(R.id.img_photos)
        }
    }

}