package com.luluforex.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.luluforex.model.PhotoResponse

@Database(entities = [PhotoResponse::class], version = 3)
abstract class PhotoDataBase : RoomDatabase() {
    abstract fun albumDao(): AlbumDao?

    companion object {
        private var instance: PhotoDataBase? = null

        @JvmStatic
        @Synchronized
        fun getInstance(context: Context): PhotoDataBase? {
            if (instance == null) {
                instance = Room.databaseBuilder(context.applicationContext,
                        PhotoDataBase::class.java, "photo_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
            }
            return instance
        }

        private val roomCallback: Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }
    }
}