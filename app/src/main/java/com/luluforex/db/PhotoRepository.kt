package com.luluforex.db

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.luluforex.db.PhotoDataBase.Companion.getInstance
import com.luluforex.model.PhotoResponse

class PhotoRepository(application: Application?) {
    private val albumDao: AlbumDao?
    val allPhotos: LiveData<List<PhotoResponse>>
    fun insert(photos: List<PhotoResponse?>) {
        InsertPhotosAsyncTask(albumDao, photos).execute()
    }

     class InsertPhotosAsyncTask(private val albumDao: AlbumDao?, var photos: List<PhotoResponse?>) : AsyncTask<Void?, Void?, Void?>() {
         override fun doInBackground(vararg params: Void?): Void? {
            albumDao!!.addPhotos(photos)
            return null
        }
     }

    init {
        val database = getInstance(application!!)
        albumDao = database!!.albumDao()
        allPhotos = albumDao!!.photos
    }
}