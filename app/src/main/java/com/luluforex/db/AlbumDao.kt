package com.luluforex.db

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.luluforex.model.PhotoResponse

@Dao
interface AlbumDao {
    @Insert
    fun addPhotos(photos: List<PhotoResponse?>?)

//    @Query("select * from photos_table")
//    fun photos(): MutableLiveData<List<PhotoResponse>?>


    @get:Query("select * from photos_table")
    val photos: LiveData<List<PhotoResponse>>
}